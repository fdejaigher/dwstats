<?php
/**
 * Options for the dwstats plugin
 *
 * @author Andreas Gohr <andi@splitbrain.org>
 */

$meta['db_server']   = array('string');
$meta['db_user']     = array('string');
$meta['db_password'] = array('password');
$meta['db_database'] = array('string');
$meta['db_prefix']   = array('string');
$meta['loggroups']   = array('array');

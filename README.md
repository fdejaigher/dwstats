# DWStats Plugin for Dokuwiki
This plugin is a fork from statistics plugin by Andreas Gohr <andi@splitbrain.org>

**This plugin is under development, use it at your own risks.**

What's different :

* Detection of Qwant
* Replacing Yahoo's links by Qwant's link in stats display
* Search engine's links open in a new window / tag

Next features

* Dokuwiki Syntax to be able to add some stats in pages.

-All documentation for this plugin can be found at (**not yet**)-
http://www.dokuwiki.org/plugin:dwstats

If you install this plugin manually, make sure it is installed in
lib/plugins/dwstats/ - if the folder is called different it
will not work!

Please refer to http://www.dokuwiki.org/plugins for additional info
on how to install plugins in DokuWiki.

----

Copyright (C)

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; version 2 of the License

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

See the COPYING file in your DokuWiki folder for details
